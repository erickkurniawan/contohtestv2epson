﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ContohTestProject2.Test
{
    public class MathHelper
    {
        public static int Add(int a, int b) => a + b;
    }

    [TestClass]
    public class MathTest
    {
        [DataTestMethod]
        [DataRow(1,1,2)]
        [DataRow(12,30,42)]
        [DataRow(1,2,3)]
        [TestMethod]
        public void test_add_multiple_data(int a,int b,int expected)
        {
            var actual = MathHelper.Add(a, b);
            Assert.AreEqual(expected, actual);
        }


        [DataTestMethod]
        [DynamicData(nameof(GetData),DynamicDataSourceType.Method)]
        [TestMethod]
        public void test_add_dynamic_data_method(int a, int b, int expected)
        {
            var actual = MathHelper.Add(a, b);
            Assert.AreEqual(expected, actual);
        }

        [DataTestMethod]
        [DynamicData("Data", DynamicDataSourceType.Property)]
        [TestMethod]
        public void test_add_dynamic_data_property(int a, int b, int expected)
        {
            var actual = MathHelper.Add(a, b);
            Assert.AreEqual(expected, actual);
        }


        //dynamic data sample
        public static IEnumerable<object[]> GetData()
        {
            yield return new object[] { 1, 2, 3 };
            yield return new object[] { 2, 5, 7 };
        }

        public static IEnumerable<object[]> Data
        {
            get
            {
                yield return new object[] { 1, 2, 3 };
                yield return new object[] { 2, 5, 7 };
            }
        }
    }

   
}
