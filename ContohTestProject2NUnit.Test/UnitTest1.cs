﻿using NUnit.Framework;
using System;


namespace ContohTestProject2NUnit.Test
{

    public enum CustomerType
    {
        Basic,Premium,SpecialCustomer
    }

    public class Order
    {
        public int OrderId { get; set; }
        public int ProductId { get; set; }
        public int ProductQuantity { get; set; }
        public decimal Amount { get; set; }
    }

    public class Customer
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public CustomerType CustomerType { get; set; }
    }

    public class ApplyDiscountProcess
    {
        public void ApplyDiscount(Customer customer, Order order)
        {
            if (customer.CustomerType == CustomerType.Premium)
            {
                order.Amount = order.Amount - ((order.Amount * 10)) / 100;
            }
            else if (customer.CustomerType == CustomerType.SpecialCustomer)
            {
                order.Amount = order.Amount - ((order.Amount * 20)) / 100;
            }
        }
    }
    
    [TestFixture(CustomerType.Premium,100)]
    //[TestFixture(CustomerType.Premium)]
    public class ApplyDiscountProcessTests
    {
        private CustomerType customerType;
        private int minOrder;

        public ApplyDiscountProcessTests(CustomerType customerType)
        {
            this.customerType = customerType;
        }

        public ApplyDiscountProcessTests(CustomerType customerType,int minOrder)
        {
            this.customerType = customerType;
            this.minOrder = minOrder;
        }


        [TestCase(CustomerType.Premium,100,90)]
        [TestCase(CustomerType.Basic,100,100)]
        public void cek_type_user_and_amount(CustomerType customerType,
            int minOrder,int amount)
        {
            Customer newCust = new Customer
            {
                CustomerId = 1,
                CustomerName = "Erick",
                CustomerType = customerType
            };

            Order order = new Order
            {
                OrderId = 1,
                ProductId = 111,
                ProductQuantity = 1,
                Amount = minOrder
            };

            ApplyDiscountProcess objDisc = new ApplyDiscountProcess();
            objDisc.ApplyDiscount(newCust, order);

            Assert.AreEqual(order.Amount, amount);
        }

        [TestCase(CustomerType.Premium,100,ExpectedResult =90)]
        [TestCase(CustomerType.Basic,100,ExpectedResult =100)]
        public decimal cek_type_user_and_amount_expected(CustomerType customerType,
            int minOrder)
        {
            Customer newCust = new Customer
            {
                CustomerId = 1,
                CustomerName = "Erick",
                CustomerType = customerType
            };

            Order order = new Order
            {
                OrderId = 1,
                ProductId = 111,
                ProductQuantity = 1,
                Amount = minOrder
            };

            ApplyDiscountProcess objDisc = new ApplyDiscountProcess();
            objDisc.ApplyDiscount(newCust, order);
            return order.Amount;
        }

        [Test]
        public void cek_premium_user_expect_10percent()
        {
            //arrange
            Customer newCust = new Customer
            {
                CustomerId = 1,
                CustomerName = "Erick",
                CustomerType = this.customerType
            };

            Order order = new Order
            {
                OrderId = 1,
                ProductId = 111,
                ProductQuantity = 1,
                Amount = this.minOrder
            };

            ApplyDiscountProcess objDisc = new ApplyDiscountProcess();
            objDisc.ApplyDiscount(newCust, order);

            Assert.AreEqual(order.Amount, 90);
        }
    }
}
